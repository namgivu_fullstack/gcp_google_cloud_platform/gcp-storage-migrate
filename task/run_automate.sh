#!/usr/bin/env bash
# TODO: we want move nnp1 @ Project1 to Project2

SH=`cd $(dirname $BASH_SOURCE) && pwd`  # SH aka SCRIPT_HOME
AH=$(cd "$SH/../" && pwd)

# load config
echo "Load config..."
source "$SH/.config.sh"
echo "Done load config!"

# Step 1: create backup bucket: BUCKET_BACKUP @ Project2
BUCKET_BACKUP="$BUCKET_MOVE-backup"
echo "Creating bucket $BUCKET_BACKUP @ $PROJECT2_ID ..."
gsutil -m -o "Credentials:gs_service_key_file=$JSON_KEY_PR2" mb -p $PROJECT2_ID -l $BUCKET_LOCATION gs://$BUCKET_BACKUP
echo "Created $BUCKET_BACKUP @ $PROJECT2_ID!"
    # Grant access BUCKET_BACKUP @ Project2
    echo "Granting access to bucket $BUCKET_BACKUP @ $PROJECT2_ID ..."
    gsutil -m -o "Credentials:gs_service_key_file=$JSON_KEY_PR2" acl ch -u AllUsers:W gs://$BUCKET_BACKUP
    echo "Granted bucket $BUCKET_BACKUP @ $PROJECT2_ID!"

# Step 2: copy objects from BUCKET_MOVE @ Project1 to BUCKET_BACKUP @ Project2
    # Public bucket BUCKET_MOVE @ Project1
    echo "Granting access to bucket $BUCKET_MOVE @ $PROJECT1_ID ..."
    gsutil -m -o "Credentials:gs_service_key_file=$JSON_KEY_PR1" acl ch -u AllUsers:R gs://$BUCKET_MOVE
    echo "Granted bucket $BUCKET_MOVE @ $PROJECT1_ID!"
    # copy object from  BUCKET_MOVE @ Project1 to BUCKET_BACKUP @ Project2
    echo "Copying $BUCKET_MOVE @ $PROJECT1_ID to $BUCKET_BACKUP @ $PROJECT2_ID ..."
    gsutil -m -o "Credentials:gs_service_key_file=$JSON_KEY_PR1" cp -r gs://$BUCKET_MOVE/*  gs://$BUCKET_BACKUP
    echo "Copied $BUCKET_MOVE @ $PROJECT1_ID to $BUCKET_BACKUP @ $PROJECT2_ID!"
    # remove public bucket BUCKET_MOVE @ Project1
    echo "Revoking access to bucket $BUCKET_MOVE @ $PROJECT1_ID ..."
    gsutil -m -o "Credentials:gs_service_key_file=$JSON_KEY_PR1" acl set private gs://$BUCKET_MOVE
    echo "Revoked access to bucket $BUCKET_MOVE @ $PROJECT1_ID!"
    # remove public bucket BUCKET_BACKUP @ Project2
    echo "Revoking access to bucket $BUCKET_BACKUP @ $PROJECT2_ID ..."
    gsutil -m -o "Credentials:gs_service_key_file=$JSON_KEY_PR2" acl set private gs://$BUCKET_BACKUP
    echo "Revoked access to bucket $BUCKET_BACKUP @ $PROJECT2_ID!"


# Step 3: get list_public and list_private of BUCKET_MOVE @ Project1
readarray list_allfiles < <(gsutil -m -o "Credentials:gs_service_key_file=$JSON_KEY_PR1" ls -r gs://$BUCKET_MOVE)
list_pubic=()
list_private=()

echo "Getting list private objects and list public objects ..."
for i in "${list_allfiles[@]}"; do
    output=`(gsutil -m -o "Credentials:gs_service_key_file=$JSON_KEY_PR1" acl get "$i" | grep "allUsers")`

    if [ -z "$output" ]; then
        output1=`(gsutil -m -o "Credentials:gs_service_key_file=$JSON_KEY_PR1" acl get "$i" | grep "allAuthenticatedUsers")`
        if [ -z "$output1" ]; then
            list_private+=("$i")
        else
            list_pubic+=("$i")
        fi
    else
        list_pubic+=("$i")
    fi
done
echo "Gotten!"

# Make sure we have list file to recover if any errors happen
echo "Storing list private and list public to file ..."
for i in "${list_private[@]}"; do
    echo "$i" >> $AH/private_files.txt
done

for i in "${list_pubic[@]}"; do
    echo "$i" >> $AH/public_files.txt
done
echo "Stored!"

# Step 3: Remove BUCKET_MOVE @ Project1 - rm -r will delete all objects first then delete bucket
echo "Deleting all objects and bucket $BUCKET_MOVE @ $PROJECT1_ID itself ..."
gsutil -m -o "Credentials:gs_service_key_file=$JSON_KEY_PR1" rm -r gs://$BUCKET_MOVE
echo "Deleted!"

# Step 4: Create BUCKET_MOVE @ Project2
echo "Creating $BUCKET_MOVE @ $PROJECT2_ID"
gsutil -m -o "Credentials:gs_service_key_file=$JSON_KEY_PR2" mb -p $PROJECT2_ID -l $BUCKET_LOCATION gs://$BUCKET_MOVE
echo "Created $BUCKET_MOVE @ $PROJECT2_ID"
    # Grant access BUCKET_MOVE @ Project2
    echo "Granting access to $BUCKET_MOVE @ $PROJECT2_ID ..."
    gsutil -m -o "Credentials:gs_service_key_file=$JSON_KEY_PR2" acl ch -u AllUsers:W gs://$BUCKET_MOVE
    echo "Granted!"

# Step 5: Copy BUCKET_BACKUP @ Project 2 to BUCKET_MOVE @ Project2
echo "Copying $BUCKET_BACKUP @ $PROJECT2_ID to $BUCKET_MOVE @ $PROJECT2_ID ..."
gsutil -m -o "Credentials:gs_service_key_file=$JSON_KEY_PR2" cp -r gs://$BUCKET_BACKUP/*  gs://$BUCKET_MOVE
echo "Copied!"
    # remove public bucket BUCKET_MOVE @ Project2
    echo "Revoking access of $BUCKET_MOVE @ $PROJECT2_ID ..."
    gsutil -m -o "Credentials:gs_service_key_file=$JSON_KEY_PR2" acl set private gs://$BUCKET_MOVE
    sleep 1
    echo "Revoked access of $BUCKET_MOVE @ $PROJECT2_ID!"

# Step 6: After moved, all objects are private, set public based on list_pubic
echo "All objects are private!"
echo "Setting to private for all private objects based on list private ..."
for i in "${list_pubic[@]}"; do
    echo "$i"
    gsutil -m -o "Credentials:gs_service_key_file=$JSON_KEY_PR2" acl ch -u AllUsers:R "$i"
done
echo "Set!"

# Step 7: Get list private and list public from $BUCKET_MOVE @ Project2 to compare
readarray list_allfiles2 < <(gsutil -m -o "Credentials:gs_service_key_file=$JSON_KEY_PR2" ls -r gs://$BUCKET_MOVE)
list_pubic2=()
list_private2=()
echo "Getting list private objects and list public objects after moved ..."
for i in "${list_allfiles2[@]}"; do
    output=`(gsutil -m -o "Credentials:gs_service_key_file=$JSON_KEY_PR2" acl get "$i" | grep "allUsers")`

    if [ -z "$output" ]; then
        output1=`(gsutil -m -o "Credentials:gs_service_key_file=$JSON_KEY_PR2" acl get "$i" | grep "allAuthenticatedUsers")`
        if [ -z "$output1" ]; then
            list_private2+=("$i")
        else
            list_pubic2+=("$i")
        fi
    else
        list_pubic2+=("$i")
    fi
done
echo "Gotten!"

echo "Comparing list public ..."
result_public=`echo ${list_pubic[@]} ${list_pubic2[@]} | tr ' ' '\n' | sort | uniq -u`
if [ -z "$result_public" ]; then
    echo "Well no diff!"
else
    echo "Some errors are happened: $result_public"
fi

echo "Comparing list private ..."
result_private=`echo ${list_private[@]} ${list_private2[@]} | tr ' ' '\n' | sort | uniq -u`
if [ -z "$result_private" ]; then
    echo "Well no diff!"
else
    echo "Some errors are happened: $result_private"
fi

echo "DONE!!!"
