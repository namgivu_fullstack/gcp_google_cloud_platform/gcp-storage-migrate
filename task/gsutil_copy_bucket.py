import subprocess
import sys


class GCS:

    @staticmethod
    def copy_bucket(path_json, bucket1, bucket2):
        try:
            """
            gsutil -m to run with multi threads for faster
            """
            subprocess.call(f'''pipenv run gsutil -m -o "Credentials:gs_service_key_file={path_json}" \
                                                  cp -r gs://{bucket1}/* gs://{bucket2}''',
                            shell=True)
        except subprocess.CalledProcessError as e:
            raise (e.output)


if __name__ == '__main__':
    """
    **permission**
    Bucket1: we must have viewer access
    Bucket2: we must have full access
    
    **sample usage**
    pipenv run python  :gsutil_copy_bucket.py  nnp1-bucket-backup/*   nnp1-bucket
    #                  .                       source bucket          destination bucket
    """
    # define arg
    path_json = 'project1-289904-c3963ecae5bd.json'
    bucket1 = sys.argv[1]
    bucket2 = sys.argv[2]

    # run copy
    GCS.copy_bucket(path_json,bucket1, bucket2)