import os
from os.path import dirname, abspath
from datetime import datetime

import src.service.gcs as GCS


"""
sample usage
./run.sh
"""

"""
# fixture prepare
prepare bucket nnp1-bucket-YmDHMS @ project1

# steps
00  backup          `nnp1-bucket-YmDHMS        @ project1` to `nnp1-bucket-YmDHMS-backup @ project1`
01  transfer/clone  `nnp1-bucket-YmDHMS        @ project1` to `nnp1-bucket-YmDHMS-cloned @ project2` - pls mind it's project2 to clone to

02a delete          `nnp1-bucket-YmDHMS        @ project1`
02b create          `nnp1-bucket-YmDHMS        @ project2`
02c transfer/rename `nnp1-bucket-YmDHMS-cloned @ project2` to `nnp1-bucket-YmDHMS @ project2`

03  publish all bucket files in `nnp1-bucket-YmDHMS @ project2`
04  done

# clean up
delete bucket nnp1-bucket-YmDHMS        @ project1
delete bucket nnp1-bucket-YmDHMS-backup @ project1
delete bucket nnp1-bucket-YmDHMS        @ project2
"""

#region envvar GOOGLE_APPLICATION_CREDENTIALS infra
'''
We have TWO storage project :project1 and :project2 with different permission/keyfile
--> must have TWO .env files for each
'''
ENV_PROJECT1=os.environ.get('ENV_PROJECT1'); assert ENV_PROJECT1
ENV_PROJECT2=os.environ.get('ENV_PROJECT2'); assert ENV_PROJECT2
#endregion envvar GOOGLE_APPLICATION_CREDENTIALS infra


#region prepare nnp1-bucket-YmDHMS @ project1
YmDHMS      = datetime.now().strftime('%Y%m%d-%H%M%S')  # timestamp
BUCKET_NAME = f'nnp1-bucket-{YmDHMS}'

print(f'GCS.create_bucket({BUCKET_NAME}, ENV_PROJECT1) ...')
GCS.create_bucket(BUCKET_NAME, from_service_account_json=ENV_PROJECT1)

print(f'GCS.upload_folder({BUCKET_NAME}, ENV_PROJECT1) ...')
AH                 = abspath(dirname(__file__)+'/..')
FIXTURE_BUCKET_DIR = f'{AH}/tests/_fixture_/sample_bucket/'
GCS.upload_folder(from_local_folder=FIXTURE_BUCKET_DIR, to_bucket_name=BUCKET_NAME, to_bucket_folder='', from_service_account_json=ENV_PROJECT1)
#endregion prepare nnp1-bucket-YmDHMS @ project1


#region 00 backup
guide00= f'''
---
TLDR
backup `{BUCKET_NAME} @ project1` 
    to `{BUCKET_NAME}-backup @ project1`

---
Full detail
Backup via Transfer feature in GPC storage webconsole
aka 
Transfer       {BUCKET_NAME}        @ project1 
     to        {BUCKET_NAME}-backup @ project1

Steps
* Go to link: https://console.cloud.google.com/transfer/cloud
* Make sure we are in the right project ie project1
* Create :transfer job with
  - Source      : {BUCKET_NAME}
  - Destination : {BUCKET_NAME}-backup
* Run & wait for completion ie > Status column: Completed
'''

GCS.create_bucket(f'{BUCKET_NAME}-backup', from_service_account_json=ENV_PROJECT1)

#region TODO automated_steps
def TODO_automated_steps():
    try:
        from src.service.webdriver.wd import load_webdriver
        from src.service.webdriver.wd_helper import wait_by_xpath

        wd = load_webdriver()
        wd.get('https://console.cloud.google.com/transfer/cloud')
        GCP_WEBCONSOLE_USER = os.environ.get('GCP_WEBCONSOLE_USER'); assert GCP_WEBCONSOLE_USER
        GCP_WEBCONSOLE_PASS = os.environ.get('GCP_WEBCONSOLE_PASS'); assert GCP_WEBCONSOLE_PASS
        wait_by_xpath(wd, "//*[@type='email']")         .send_keys(GCP_WEBCONSOLE_USER)
        wait_by_xpath(wd, "(//*[@type='button'])[3]")   .click()
        #TODO cannot proceed - webdriver browser results as 'webbrowser not safe to use' - this may be an on-purpose ban by GCP from using webdriver to open webconsole
    finally:
        wd.quit()
# automated_steps()
#endregion TODO automated_steps

input(f'''
{guide00}
---
Please follow the steps above
Press :enter when done ...
''')
#endregion 00 backup


#region 01 transfer/clone
GCS.create_bucket(f'{BUCKET_NAME}-clone', from_service_account_json=ENV_PROJECT2)  # mind we work with :project2 here

guide01=f'''
01 transfer/clone `{BUCKET_NAME}       @ project1` 
               to `{BUCKET_NAME}-clone @ project2`
               pls mind 00 it's project2 to clone to
                        01 pre-create bucket {BUCKET_NAME}-clone in :project2
                        02 MUST maually enter {BUCKET_NAME}-clone, NOT select from listed bucket (will only display buckets in :project1)
'''

input(f'''
{guide01}
---
Please follow the steps above
Press :enter when done ...
''')

#endregion 01 transfer/clone


#region 02a 02b delete@p1 rename@p2
'''
02a delete          `nnp1-bucket-YmDHMS        @ project1`
02b create          `nnp1-bucket-YmDHMS        @ project2`
02c transfer/rename `nnp1-bucket-YmDHMS-cloned @ project2` to `nnp1-bucket-YmDHMS @ project2`
'''

guide02=f'''
02a delete          `{BUCKET_NAME}       @ project1`
02b create          `{BUCKET_NAME}       @ project2`
02c transfer/rename `{BUCKET_NAME}-clone @ project2` to `{BUCKET_NAME} @ project2` - pls mind to SELECT project as :project2
'''
print(f'GCS.delete_bucket({BUCKET_NAME}, ENV_PROJECT1) ...'); GCS.delete_bucket(f'{BUCKET_NAME}', from_service_account_json=ENV_PROJECT1)
print(f'GCS.create_bucket({BUCKET_NAME}, ENV_PROJECT2) ...'); GCS.create_bucket(f'{BUCKET_NAME}', from_service_account_json=ENV_PROJECT2)

input(f'''
{guide02}
---
Please follow the steps above
Press :enter when done ...
''')
print(f'GCS.delete_bucket({BUCKET_NAME}-clone, ENV_PROJECT2) ...'); GCS.delete_bucket(f'{BUCKET_NAME}-clone', from_service_account_json=ENV_PROJECT2)

#endregion 02a 02b delete@p1 rename@p2


# 03  publish all bucket files in `nnp1-bucket-YmDHMS @ project2`
print(f'GCS.make_public({BUCKET_NAME}, ENV_PROJECT2)')
GCS.make_public(BUCKET_NAME, from_service_account_json=ENV_PROJECT2)

input('''
---
gcs_migrate_bucket steps done here
Pls :enter to proceed ending...
''')

#region aftermath
'''
TODO ensure file list in       `nnp1-bucket-YmDHMS        @ project2`
                      equal to `nnp1-bucket-YmDHMS-backup @ project2`
                      equal to :FIXTURE_BUCKET_DIR
TODO test download a public url
'''
#endregion aftermath


#region clean up
print('''
---
clean up
''')
print(f'GCS.delete_bucket({BUCKET_NAME}-backup, ENV_PROJECT1) ...'); GCS.delete_bucket(f'{BUCKET_NAME}-backup', from_service_account_json=ENV_PROJECT1)
print(f'GCS.delete_bucket({BUCKET_NAME},        ENV_PROJECT2) ...');        GCS.delete_bucket(f'{BUCKET_NAME}',        from_service_account_json=ENV_PROJECT2)
#endregion clean up
