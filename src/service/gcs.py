from google.cloud import storage
from os import walk
import re
import subprocess


"""
ref. https://github.com/googleapis/python-storage
"""

def _get_client(from_service_account_json=None):  # load serviceaccount from file at this path :from_service_account_json
    if from_service_account_json: c = storage.Client.from_service_account_json(from_service_account_json)
    else:                         c = storage.Client()
    return c


def create_bucket(bucket_name, from_service_account_json=None, project=None, location='asia-southeast1'):
    """
    :param bucket_name:
    :param from_service_account_json:
    :param project: must be project-id, not name
    :param location:
    :return:
    """
    c = _get_client(from_service_account_json)
    c.create_bucket(bucket_or_name=bucket_name, project=project, location=location)


def delete_bucket(bucket_name, from_service_account_json=None, skip_ifnotexists=False):
    """
    ref. https://github.com/googleapis/python-storage/blob/master/tests/unit/test_bucket.py#L1036
        ref. google.cloud.storage.bucket.Bucket.delete()
    """
    try:
        c = _get_client(from_service_account_json)
        b = c.bucket(bucket_name=bucket_name)
        b.delete(force=True)
    except:
        if skip_ifnotexists: pass
        else: raise


def upload_folder(
    from_local_folder, to_bucket_name, to_bucket_folder:'empty str to upload to bucket root',
    from_service_account_json=None,
    verbose=True,
):
    """
    Upload :local_folder to bucket
    NOTE folder must have at least one file
    """
    c      = _get_client(from_service_account_json)
    bucket = c.get_bucket(to_bucket_name)

    for dir_fullpath, _, files in walk(from_local_folder):
        sub_dir            = dir_fullpath.replace(from_local_folder, '')
        if to_bucket_folder: bucket_folder_path = f'{to_bucket_folder}/{sub_dir}/'
        else:                bucket_folder_path = f'{sub_dir}/'

        for f in files:
            dest_filepath = bucket_folder_path + '/' + f
            src_filepath  = dir_fullpath       + '/' + f

            # replace repeated / into single /
            dest_filepath = re.sub(r'/+', '/', dest_filepath)
            src_filepath  = re.sub(r'/+', '/', src_filepath)

            b = bucket.blob(dest_filepath); b.upload_from_filename(src_filepath)  # dest_xx vs src_xx when upload blob ref. https://stackoverflow.com/a/56353765/248616
            b.make_public()

            if verbose:
                print(f'{src_filepath}-->{dest_filepath}')
                print(f'[GoogleApi][Storage][{to_bucket_name}][{bucket_folder_path}] File {f} uploaded  at {b.public_url}')


#region public/privateblob
def make_public(bucket_name, from_service_account_json=None, verbose=False):
    """Makes blob/file in :bucket_name publicly accessible."""
    c     = _get_client(from_service_account_json)
    b_all = c.list_blobs(bucket_name)  # b_all aka blob_all
    for b in b_all:
        b.make_public()
        if verbose: print(f'[GoogleApi][Storage] Blob {b.name} is publicly accessible at {b.public_url}')


def get_public_private_blob(bucket_name, from_service_account_json=None):
    c = _get_client(from_service_account_json)
    try:
        list_public = []
        list_private = []
        blobs = c.list_blobs(bucket_name)
        for blob in blobs:
            list_entities = str(blob.acl.get_entities())
            if 'allAuthenticatedUsers' in list_entities or 'allUsers' in list_entities:
                list_public.append(blob.name)
            else:
                list_private.append(blob.name)
        return list_public, list_private
    except Exception as e:
        raise e


def make_public_blob(bucket_name, blobs, from_service_account_json=None):
    """Makes a blob publicly accessible."""
    c = _get_client(from_service_account_json)
    try:
        bucket = c.bucket(bucket_name)
        for bln in blobs:  # bln aka blob name
            blob = bucket.get_blob(bln)
            blob.make_public()
            print(f'[GoogleApi][Storage] Blob {blob.name} is publicly accessible at {blob.public_url}')
    except Exception as e:
        print('[GoogleApi][Storage] Error making file public: ', e)
        raise


def make_private_blob(bucket_name, blobs, from_service_account_json=None):
    """Makes a blob publicly accessible."""
    c = _get_client(from_service_account_json)
    try:
        bucket = c.bucket(bucket_name)
        for bln in blobs:  # bln aka blob name
            blob = bucket.get_blob(bln)
            blob.make_private()
            print(f'[GoogleApi][Storage] Blob {blob.name} is private')
    except Exception as e:
        print('[GoogleApi][Storage] Error making file private: ', e)
        raise
#endregion public/privateblob


def copy_bucket(from_bucket, to_bucket, json_serviceaccount_file):
    subprocess.call(
        f'pipenv run gsutil -m  -o "Credentials:gs_service_key_file={json_serviceaccount_file}"  cp -r gs://{from_bucket}/* gs://{to_bucket}',
        #            .          json keyfile                                      .     .                .
        #            .      -m multi threads to run faster
        shell=True  #TODO what is this param for?
    )
