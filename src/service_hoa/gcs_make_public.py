import sys
from google.cloud import storage


"""
Usage:
    pipenv run python  gg_make_public.py :bucket_name
"""


class Storage:

    @staticmethod
    def get_public_private_blob(bucket_name):
        storage_client = storage.Client()
        try:
            list_public = []
            list_private = []
            blobs = storage_client.list_blobs(bucket_name)
            for blob in blobs:
                list_entities = str(blob.acl.get_entities())
                if 'allAuthenticatedUsers' in list_entities or 'allUsers' in list_entities:
                    list_public.append(blob.name)
                else:
                    list_private.append(blob.name)
            return list_public, list_private
        except Exception as e:
            raise e

    @staticmethod
    def make_public(bucket_name, blobs):
        """Makes a blob publicly accessible."""
        storage_client = storage.Client()
        try:
            bucket = storage_client.bucket(bucket_name)
            for bln in blobs:  # bln aka blob name
                blob = bucket.get_blob(bln)
                blob.make_public()
                print(f'[GoogleApi][Storage] Blob {blob.name} is publicly accessible at {blob.public_url}')
        except Exception as e:
            print('[GoogleApi][Storage] Error making file public: ', e)
            raise

    @staticmethod
    def make_private(bucket_name, blobs):
        """Makes a blob publicly accessible."""
        storage_client = storage.Client()
        try:
            bucket = storage_client.bucket(bucket_name)
            for bln in blobs:  # bln aka blob name
                blob = bucket.get_blob(bln)
                blob.make_private()
                print(f'[GoogleApi][Storage] Blob {blob.name} is private')
        except Exception as e:
            print('[GoogleApi][Storage] Error making file private: ', e)
            raise

if __name__ == '__main__':
    bucket_name_src = sys.argv[1]
    bucket_name_des = sys.argv[2]
    # load list public and list private
    list_public, list_private = Storage.get_public_private_blob(bucket_name_src)
    # make public
    Storage.make_public(bucket_name_des, list_public)
    # make private
    Storage.make_private(bucket_name_des, list_private)

