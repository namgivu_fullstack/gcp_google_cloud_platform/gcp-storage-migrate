from tests._common_ import FIXTURE_HOME


def test():
    print()
    print(f"{'sub_dir':<22}   {'files':<12}   {'dir_fullpath'}")

    local_folder = f'{FIXTURE_HOME}/sample_bucket/'
    from os import walk
    for dir_fullpath, subdir_list, files in walk(local_folder):
        '''print w/ column width ref. https://stackoverflow.com/a/8450514/248616'''
        print(f'{str(subdir_list):<22}   {str(files):<12}   {str(dir_fullpath)}')
