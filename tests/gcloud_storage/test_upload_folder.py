import src.service.gcs as GCS
from tests._common_ import FIXTURE_HOME

"""
fixture prepare [ref](:gcp-storage-migrate.private/doc/fixture prepare.md)
"""


def test__w_nn_keyfile():
    """
    require nn200930's gcs project-owner keyfile
    """
    GCS.upload_folder(
        to_bucket_name    = 'nnp1-bucket',
        from_local_folder = f'{FIXTURE_HOME}/sample_bucket/',
        to_bucket_folder  = 'test_upload_folder/',
    )
