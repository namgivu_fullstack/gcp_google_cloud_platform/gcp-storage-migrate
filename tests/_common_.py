from os.path import dirname

SH           = dirname(__file__)  # SH aka script_home
FIXTURE_HOME = f'{SH}/_fixture_'
